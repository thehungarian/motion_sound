#include "FilePlayer.h"

#include <Arduino.h>

bool FilePlayer::play()
{
  if (!isOpen())
  {
    return false;
  }

  bool result = true;

  //check for the string WAVE starting at 8th bit in header of file
  seek(8);
  const char wavStr[] = "WAVE";
  for (int count = 0; count < strlen(wavStr); count++)
  {
    if (read() != wavStr[count])
    {
      Serial.println("Not a wav file");
      result = false;
      break;
    }
  }


  if (result)
  {
    seek(24);

    uint32_t sample_rate {};
    sample_rate = read();
    sample_rate |= read() << 8;
    sample_rate |= read() << 16;
    sample_rate |= read() << 24;
    Serial.print("Sample Rate: ");
    Serial.println(sample_rate);

    uint32_t byte_rate = read();
    byte_rate |= read() << 8;
    byte_rate |= read() << 16;
    byte_rate |= read() << 24;
    Serial.print("Byte rate: ");
    Serial.println(byte_rate);

    uint16_t bits_per_sample = read();
    bits_per_sample |= read() << 8;
    Serial.print("Bits per sample: ");
    Serial.println(bits_per_sample);


    DACSetup(sample_rate, 2);

    if (result && sample_rate > 0)
    {
      byte buffer[1024 * 4] {};
      // skip the tag data
      seek(40); // get the data size
      uint32_t sample_size = read();
      sample_size |= read() << 8;
      sample_size |= read() << 16;
      sample_size |= read() << 24;
      Serial.print("Sample size: ");
      Serial.println(sample_size);

      do {
        uint32_t bytes_read = read(buffer, sizeof(buffer));
        sample_size -= bytes_read;

        if (bytes_read > 0)
        {
          playSample(buffer, bytes_read);

        } else {
          break;
        }
      } while (sample_size > 0);

    }

  }

  return result;
}

// Uses GCLK3 and TC4, affects TC5

const uint8_t *sampleName;
uint32_t sampleSize;
uint8_t overSampling;
bool playFinished = true;

void FilePlayer::DACSetup(uint32_t sampleFreq, uint8_t overSamp) {
  if (overSamp != 1 && overSamp != 2 && overSamp != 4) {  // If oversampling is not set to 1, 2 or 4,
    overSampling = 1;                                     // default to 1
  } else {                                                // else
    overSampling = overSamp;                              // set oversampling to 1, 2 or 4
  }
  uint32_t top = 47972352 / (sampleFreq * overSampling);  // Calculate the TOP value
  REG_GCLK_GENDIV = GCLK_GENDIV_DIV(1) |                  // Divide the 48MHz clock source by 1 for 48MHz
                    GCLK_GENDIV_ID(3);                    // Select GCLK3
  while (GCLK->STATUS.bit.SYNCBUSY);                      // Wait for synchronization

  REG_GCLK_GENCTRL = GCLK_GENCTRL_IDC |                   // Set the duty cycle to 50/50
                     GCLK_GENCTRL_GENEN |                 // Enable GCLK3
                     GCLK_GENCTRL_SRC_DFLL48M |           // Set the 48MHz clock source
                     GCLK_GENCTRL_ID(3);                  // Select GCLK3
  while (GCLK->STATUS.bit.SYNCBUSY);                      // Wait for synchronization

  REG_GCLK_CLKCTRL = GCLK_CLKCTRL_CLKEN |                 // Enable clock
                     GCLK_CLKCTRL_GEN_GCLK3 |             // Select GCLK3
                     GCLK_CLKCTRL_ID_TC4_TC5;             // Feed the GCLK3 to TC4 and TC5
  while (GCLK->STATUS.bit.SYNCBUSY);                      // Wait for synchronization

  REG_TC4_COUNT16_CC0 = top;                              // Set the TC4 CC0 register to top
  while (TC4->COUNT16.STATUS.bit.SYNCBUSY);               // Wait for synchronization

  NVIC_SetPriority(TC4_IRQn, 0);                          // Set the interrupt priority for TC4 to 0 (highest)
  NVIC_EnableIRQ(TC4_IRQn);                               // Connect TC4 to Nested Vector Interrupt Controller

  REG_TC4_INTFLAG |= TC_INTFLAG_OVF;                      // Clear the interrupt flags
  REG_TC4_INTENSET = TC_INTENCLR_OVF;                     // Enable TC4 interrupts

  REG_TC4_CTRLA |= TC_CTRLA_PRESCALER_DIV1 |              // Set prescaler to 1 for 48MHz
                   TC_CTRLA_WAVEGEN_MFRQ;                 // Put the timer TC4 into Match Frequency Mode
  while (TC4->COUNT16.STATUS.bit.SYNCBUSY);               // Wait for synchronization
}

void FilePlayer::playSample(const uint8_t *name, const uint32_t size) {

  while (!playFinished)
  {
    delay(1);
  }

  playFinished = false;
  sampleName = name;                                      // Set global variables
  sampleSize = size;                                      // for interrupt handler function
  REG_TC4_CTRLA |= TC_CTRLA_ENABLE;                       // Enable timer TC4
  while (TC4->COUNT16.STATUS.bit.SYNCBUSY);               // Wait for synchronization

}

inline void DACWrite(uint16_t sample) {
  DAC->DATA.reg = sample;                               // Shortened version
  while (ADC->STATUS.bit.SYNCBUSY);                     // of the code used
  DAC->CTRLA.bit.ENABLE = 0x01;                         // in analogWrite()
  while (ADC->STATUS.bit.SYNCBUSY);                     // in wiring_analog.c
}

void TC4_Handler() {                                      // Interrupt Service Routine for timer TC4
  static uint16_t currentSample, previousSample = 0;
  static uint8_t sampleInterruptCounter = 0;
  static uint32_t sampleNumber = 1;

  if (sampleInterruptCounter == 0) {                      // If this is the first pass for this sample:
    currentSample = sampleName[sampleNumber];             // Get the current sample value
    previousSample = sampleName[sampleNumber - 1];        // Get the previous sample value
    currentSample <<= 2;                                  // Go to 10 bits for calculations,
    previousSample <<= 2;                                 // and also for sending to the DAC
  }

  sampleInterruptCounter++;                               // Increment the interrupt counter

  if (sampleInterruptCounter >= overSampling) {           // When interpolation has been handled:
    DACWrite(currentSample);                              // Send the current sample to the DAC
    sampleInterruptCounter = 0;                           // Reset the interrupt counter
    sampleNumber++;                                       // Go to the next sample
    if (sampleNumber >= sampleSize) {                     // At the end of the samples array:
      REG_TC4_CTRLA &= ~TC_CTRLA_ENABLE;                  // Disable timer TC4
      while (TC4->COUNT16.STATUS.bit.SYNCBUSY);           // Wait for synchronization
      sampleNumber = 1;                                   // Reset sample number to second sample
      playFinished = true;
    }
  } else if (sampleInterruptCounter << 1 == overSampling) {     // For 2x and 4x oversampling: middle interpolation
    DACWrite((currentSample + previousSample) >> 1);
  } else if (sampleInterruptCounter << 2 == overSampling) {     // For 4x oversampling: first interpolation
    DACWrite((currentSample + (3 * previousSample)) >> 2);
  } else if (sampleInterruptCounter == 3) {                     // For 4x oversampling: third interpolation
    DACWrite(((3 * currentSample) + previousSample) >> 2);
  }

  REG_TC4_INTFLAG = TC_INTFLAG_OVF;                       // Clear the OVF interrupt flag
}
