#include <SdFat.h>

class FilePlayer : public FsFile
{
public:
  bool play();
protected:
    
  void DACSetup(uint32_t sampleFreq, uint8_t overSamp);
  void playSample(const uint8_t *name, const uint32_t size);
};
