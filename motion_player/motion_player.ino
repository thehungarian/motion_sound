#include <SdFat.h>
#include <sdios.h>
#include "FilePlayer.h"

// SD card chip select pin.
const uint8_t SD_CS_PIN = 3;

// Try to select the best SD card configuration.
#if HAS_SDIO_CLASS
#define SD_CONFIG SdioConfig(FIFO_SDIO)
#elif ENABLE_DEDICATED_SPI
#define SD_CONFIG SdSpiConfig(SD_CS_PIN, DEDICATED_SPI)
#else  // HAS_SDIO_CLASS
#define SD_CONFIG SdSpiConfig(SD_CS_PIN, SHARED_SPI)
#endif  // HAS_SDIO_CLASS

#include <Wire.h>
#include <Adafruit_MSA301.h>
#include <Adafruit_Sensor.h>

static ArduinoOutStream cout(Serial);
Adafruit_MSA301 msa;
SdFs sd;
FsFile dir;
#define MAX_FILES 32
uint32_t fileIds[MAX_FILES] {};
uint32_t numFiles {};

bool EndsWith(const char* value, const char* ending)
{
  const size_t value_len = strlen(value);
  const size_t ending_len = strlen(ending);
  
  if(value_len >= ending_len)
  {
    return strcmp(&value[value_len - ending_len], ending) == 0;
  } else {
    return false;
  }
}

bool FindFiles(FsFile& dir)
{
  FsFile file;
  numFiles = 0;

  // Open next file in root.
  // Warning, openNext starts at the current position of dir so a
  // rewind may be necessary in your application.
  while (file.openNext(&dir, O_RDONLY)) 
  {
    char fileName[1024] {};
    const size_t fileNameLen = file.getName(fileName, sizeof(fileName));
    
    if (file.isFile() && EndsWith(fileName, ".wav")) {
      file.printName(&Serial);
      // store the nth number of the dir
      fileIds[numFiles] = file.dirIndex();
      numFiles++;

      if(numFiles >= MAX_FILES)
      {
        // out of space
        break;
      }
    }
    Serial.println();
    file.close();
  }
  
  if (dir.getError()) 
  {
    Serial.println("openNext failed");
  } else {
    Serial.print("Found ");
    Serial.print(numFiles);
    Serial.println(" wave files");
  }
}

//------------------------------------------------------------------------------
void errorPrint() {
  if (sd.sdErrorCode()) {
    cout << F("SD errorCode: ") << hex << showbase;
    printSdErrorSymbol(&Serial, sd.sdErrorCode());
    cout << F(" = ") << int(sd.sdErrorCode()) << endl;
    cout << F("SD errorData = ") << int(sd.sdErrorData()) << endl;
  }
}

void setup() {
  randomSeed(analogRead(2));
  
  Serial.begin(115200);
  /*while (!Serial) {
    SysCall::yield(); // will pause Zero, Leonardo, etc until serial console opens
  }*/
  cout << "INFO: Setting up SD card..." << endl;

  if(!sd.begin(SD_CONFIG))
  {
    
    cout << F(
           "ERR: \nSD initialization failed.\n"
           "Do not reformat the card!\n"
           "Is the card correctly inserted?\n"
           "Is there a wiring/soldering problem?\n");
    if (isSpi(SD_CONFIG)) {
      cout << F(
           "Is SD_CS_PIN set to the correct value?\n"
           "Does another SPI device need to be disabled?\n"
           );
    }
    errorPrint();
    sd.initErrorHalt(&Serial);
  }

  cout << "INFO: Opening root..." << endl;
  if(!dir.open("/"))
  {
    cout << "ERR: dir.open failed" << endl;
    errorPrint();
  }

  cout << "INFO: Looking for WAV files..." << endl;
  if(!FindFiles(dir))
  {
    cout << "ERR: Failed to find wav files" << endl;
  }
  
  // Try to initialize!
  if (! msa.begin()) {
    cout << "Failed to find MSA301 chip" << endl;
    while (1) { delay(10); }
  }
  
  cout << "Found MSA301!" << endl;

  msa.setPowerMode(MSA301_NORMALMODE);
  msa.setDataRate(MSA301_DATARATE_1000_HZ);
  msa.setBandwidth(MSA301_BANDWIDTH_500_HZ);
  msa.setRange(MSA301_RANGE_2_G);
  msa.setResolution(MSA301_RESOLUTION_14 );

  msa.setClick(false, false, MSA301_TAPDUR_250_MS, 25);
  
  msa.enableInterrupts(true, true, // single, double
    true, true, true, // activeX, activeY, active z
    false, // new data
    true, false // freefall, orient
    );  // enable single/double tap
  cout << "INFO: Motion player ready." << endl;
   
}

void loop()
{
  bool playFile = false;
  
  const uint8_t orientMask = 1 << 6;
  const uint8_t doubleTapMask = 1 << 4;
  const uint8_t singleTapMask = 1 << 5;
  const uint8_t activityMask = 1 << 2;
  const uint8_t freefallMask = 1 << 0;
  
  uint8_t motionstat = msa.getMotionInterruptStatus();
  if (motionstat) {
    Serial.print("Stat:"); Serial.println(motionstat, HEX); 
    Serial.print("Tap detected (0x"); Serial.print(motionstat, HEX); Serial.println(")");
    if (motionstat & singleTapMask) {
      Serial.println("\t***Single tap");
    }
    if (motionstat & doubleTapMask) {
      Serial.println("\t***Double tap");
    }
    if(motionstat & activityMask){
      Serial.println("\t***Activity");
      playFile = true;
    }
    if(motionstat & freefallMask){
      Serial.println("\t***Freefall");
    }
    Serial.println("");
  }
  
  if(numFiles > 0 && playFile)
  {
    const uint32_t which_file = random() % numFiles;
    
    cout << "INFO: Opening file number " << which_file << endl;
    FilePlayer file;
    if(file.open(&dir, fileIds[which_file], O_RDONLY))
    {
      file.play();
      file.close();
    } else {
      cout << "ERR: Failed to open file number " << which_file << endl;
      errorPrint();
      delay(10000);
    }
  }

  SysCall::yield();
  //delay(1000);
}
