# Motion Player

Use accelerometer to trigger different sounds.

[Seeeduino XIAO](https://wiki.seeedstudio.com/Seeeduino-XIAO/)
[Adafruit STEMMA Speaker](https://www.adafruit.com/product/3885#description)
[MSA301](https://shop.pimoroni.com/products/msa301-3dof-motion-sensor-breakout)
[SD Card Reader](https://www.ebay.co.uk/itm/193453320566)

This code is very rough, it just reacts to some amount of movement. A random file is chosen.
# Arduino Setup

`File -> Preference -> Aditional Boards Manager Urls` add
https://files.seeedstudio.com/arduino/package_seeeduino_boards_index.json

Open Board manager and install "Seeed SAMD Boards"

Install the libraries:
Adafruit MSA301
Adafruit Zero I2S Library or SAMD21 Audio or seeed arduino audio

Seeed XIAO Fritzing part from this [forum post](https://forum.fritzing.org/t/seeeduino-xiao-anyone-got-the-part/10820)